package ru.csu.task4springcontext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ru.csu.task4springcontext.service.HelloService;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@SpringBootTest
@ActiveProfiles("logger-render")
class Task4SpringContextApplicationTests {
	@Autowired
	private HelloService helloService;

	@Test
	void contextLoads() {
	}

	@Test
	void sayHelloTest() {
		helloService.sayHello();
	}
}
