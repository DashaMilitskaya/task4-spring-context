package ru.csu.task4springcontext.service.impl;

import org.springframework.stereotype.Component;
import ru.csu.task4springcontext.service.MessageRenderer;

@Component
public class MessageErrorRender implements MessageRenderer {
    @Override
    public void render(String message) {
        System.err.println(message);
    }
}
